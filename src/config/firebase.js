// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC0uOnLtwK1qQ0gbm82yvD6YmGRVWofhr4",
  authDomain: "student-db-87c3c.firebaseapp.com",
  projectId: "student-db-87c3c",
  storageBucket: "student-db-87c3c.appspot.com",
  messagingSenderId: "610464173657",
  appId: "1:610464173657:web:d2b4711e723fccb7fceb48"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export default app;