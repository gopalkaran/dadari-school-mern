import React from 'react';
import './App.css';
import EnrollStudent from './components/EnrollStudent';

function App() {
  return (
    <div className="App">
      <EnrollStudent />
    </div>
  );
}

export default App;
