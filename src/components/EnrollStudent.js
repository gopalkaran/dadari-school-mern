import React, {useState} from 'react';
import app from "../config/firebase";
import { getFirestore, doc , setDoc } from "firebase/firestore";

const EnrollStudent = () => {
    const [student, setStudent] = useState({
        name : '',
        roll : '',
        mobile : '',
        address : '',
        class : ''
    })
    const [status, setStatus] = useState('');
    const onChangeHandler = (e) => {
        setStudent({...student, [e.target.name]: e.target.value});
    }

    const db = getFirestore(app);

    const onSubmitHandler =async (e) =>{
        e.preventDefault();
        console.log(student);
        if(student.name==='' || student.roll==='' || student.mobile==='' || student.address==='' || student.class==='' )
            return;
        try{
            await setDoc(doc(db, "studentdb", `C${student.class}R${student.roll}`), student); 
            setStudent({name:"", roll:"", mobile:"", address:"", class:""});
            setStatus('Your details are stored successfully')
            setTimeout(() => {
                setStatus('')
            }, 3000);
        }
        catch(err){
            console.log(err);
            setStatus('Failed to store details')
            setTimeout(() => {
                setStatus('')
            }, 3000);
        }
    }


    return (
        <div>
            {status && <div>{status}</div>}
            <form onSubmit={onSubmitHandler}>
                <div className="form-group">
                    <label htmlFor="name" className="form-label">Name</label>
                    <input type="text" name="name" onChange={onChangeHandler} className="form-input" value={student.name} />
                </div>
                <div className="form-group">
                    <label htmlFor="roll" className="form-label">Roll No</label>
                    <input type="text" name="roll" onChange={onChangeHandler} className="form-input" value={student.roll} />
                </div>
                <div className="form-group">
                    <label htmlFor="mobile" className="form-label">Mobile</label>
                    <input type="text" name="mobile" onChange={onChangeHandler} className="form-input" value={student.mobile} />
                </div>
                <div className="form-group">
                    <label htmlFor="address" className="form-label">Address</label>
                    <input type="text" name="address" onChange={onChangeHandler} className="form-input" value={student.address} />
                </div>
                <div className="form-group">
                    <label htmlFor="class" className="form-label">Class</label>
                    <input type="text" name="class" onChange={onChangeHandler} className="form-input" value={student.class} />
                </div>
                <div>
                    <input type="submit" value="Submit" />
                </div>

            </form>
        </div>
    )
}

export default EnrollStudent
